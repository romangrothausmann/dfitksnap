################################################################################
# base system
################################################################################
FROM ubuntu:16.04 as system

RUN apt-get update && apt-get install -y --no-install-recommends \
    libglew1.13 libxt6 libglu1-mesa libqt4-opengl \
    libgl1-mesa-glx libgl1-mesa-dri \
    xterm mesa-utils && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/*

ENV USERNAME diUser
RUN useradd -m $USERNAME && \
    echo "$USERNAME:$USERNAME" | chpasswd && \
    usermod --shell /bin/bash $USERNAME


################################################################################
# builder
################################################################################
FROM ubuntu:16.04 as builder

RUN apt-get update && apt-get install -y --no-install-recommends \
    git \
    ca-certificates `# essential for git over https` \
    cmake \
    build-essential

RUN apt-get update && apt-get install -y --no-install-recommends \
    libglew-dev libxt-dev \
    mesa-common-dev


RUN git clone --depth 1 -b v6.1.0 https://gitlab.kitware.com/vtk/vtk.git

RUN mkdir -p VTK_build && \
    cd VTK_build && \
    cmake \
    	  -DCMAKE_INSTALL_PREFIX=/opt/vtk/ \
	  -DCMAKE_BUILD_TYPE=Release \
	  -DBUILD_SHARED_LIBS=ON \
	  -DBUILD_TESTING=OFF \
	  -DBUILD_EXAMPLES=OFF \
	  -DVTK_RENDERING_BACKEND=OpenGL2 \
	  -DVTK_OPENGL_HAS_EGL=ON \
	  -DCMAKE_CXX_FLAGS=-DGLX_GLXEXT_LEGACY `# https://stackoverflow.com/questions/28761702/getting-error-glintptr-has-not-been-declared-when-building-vtk-on-linux ` \
	  -DVTK_Group_Qt=OFF \
	  -Wno-dev \
	  ../vtk && \
    make -j"$(nproc)" && \
    make -j"$(nproc)" install


RUN git clone -b v4.5.2 https://github.com/InsightSoftwareConsortium/ITK.git # no --depth 1 because of later cherry-pick
RUN git config --global user.email "docker@home" && git config --global user.name "docker"
RUN cd ITK && git cherry-pick 7f54e86422092a01a7ce2c836b44d7e583c9b370 `# for gcc-5: https://lists.debian.org/debian-med/2015/05/msg00039.html`

RUN mkdir -p ITK_build && \
    cd ITK_build && \
    cmake \
    	  -DCMAKE_INSTALL_PREFIX=/opt/itk/ \
	  -DCMAKE_MODULE_PATH=/opt/vtk/lib/cmake \
	  -DCMAKE_BUILD_TYPE=Release \
	  -DBUILD_SHARED_LIBS=ON \
	  -DBUILD_TESTING=OFF \
	  -DBUILD_EXAMPLES=OFF \
	  ../ITK && \
    make -j"$(nproc)" && \
    make -j"$(nproc)" install


RUN apt-get update && apt-get install -y --no-install-recommends \
    qt4-default libqt4-opengl-dev file

COPY itksnap/ itksnap/

RUN mkdir -p itksnap_build && \
    cd itksnap_build && \
    cmake \
    	  -DCMAKE_INSTALL_PREFIX=/opt/itksnap/ \
	  -DCMAKE_MODULE_PATH=/opt/itk/lib/cmake \
	  -DCMAKE_BUILD_TYPE=Release \
	  -DSNAP_USE_QT4=ON \
	  ../itksnap && \
    make -j"$(nproc)" && \
    LD_LIBRARY_PATH=/opt/itk/lib/:/opt/vtk/lib/ \
      make -j"$(nproc)" install


################################################################################
# merge
################################################################################
FROM system

COPY --from=builder /opt/vtk/ /opt/vtk/
COPY --from=builder /opt/itk/ /opt/itk/
COPY --from=builder /opt/itksnap/ /opt/itksnap/

ENTRYPOINT ["/opt/itksnap/bin/itksnap"]
